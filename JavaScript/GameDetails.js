$( document ).ready(function() {
	$('#start_attack').on('submit', function (e) {
	    e.preventDefault();

	    var data = $('#game_id').serializeArray();
	  	data = convertFormToJson(data);
	  	route("api/game/run-attack", "POST", data, "", true);

	  	setTimeout(function() {
  			listAllBattles();
		}, 1000);
	});

	$('#reset_game').on('submit', function (e) {
	    e.preventDefault();

	    var gameId = $('#game_id').val();
	  	route("api/game/reset/"+gameId, "POST", "", "", true);

	  	setTimeout(function() {
  			window.location.replace("game-log.html");
		}, 1000);
	});
});


 	
function showGameDetails() {
	var game = getFromSession("game");
	$("#game_id").val(game.id);
	$("#game-name").append(game.name);
}

function listAllBattles() {
	var game = getFromSession("game");
	showArmies(game.armies);
	showLogs(game.battleLogs);
	showStatus(game.status);
	showTurns(game.battleLogs[0].turn_number);
}


function showLogs(battleLogs) {
	$.each(battleLogs, function (index, item) {
		var builder = openRow();
		builder += appendInCell(item.is_succeed);
		builder += appendInCell(item.attacker.name);
		builder += appendInCell(item.attacker.attack_strategy);
		builder += appendInCell(item.attacker.units);

		builder += appendInCell(item.attacker.name);
		builder += appendInCell(item.units_before_attack);
		builder += appendInCell(item.units_after_attack);

		builder += closeRow();

		$("#logs").append(builder);
	})
}

function showTurns(turns) {
	$("#turns_left").text(turns);
}

function showStatus(status) {
	$('#game_status').text(status);
}

function showArmies(armies) {
	$.each(armies, function (index, item) {
		$("#armies").append("<li>"+item.name+"-"+item.units+"</li>");
	})
}

function openRow() {
	return "<tr>";
}

function closeRow() {
	return "</tr>";
}

function appendInCell(property) {
	return "<td>"+property+"</td>";
}