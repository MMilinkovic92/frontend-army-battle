$( document ).ready(function() {
	$('#create_army_form').on('submit', function (e) {
	    e.preventDefault();

	    var data = $('#create_army_form').serializeArray();
	  	data = convertFormToJson(data);

	  	data = route("api/army/create", "POST", data);
	  	console.log(data);
	});
});