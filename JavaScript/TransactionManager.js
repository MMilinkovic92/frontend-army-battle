
function route(routeName, method, jsonData, expected = "", isRunAttack = false) {
	var baseRoute = "http://army-battle.test/";
	routeName = baseRoute+routeName;

	$.ajax({
		url: routeName,
		type: method,
		data: jsonData,
		headers: {
			'Content-Type' : 'application/json'
		},
		success: function (data) { 
			if (expected != "") {
				if (expected == "game"){
					sessionStorage.setItem(expected, JSON.stringify(data));	
				}
				sessionStorage.setItem(expected, JSON.stringify(data[expected]));
			}
			if (isRunAttack) {
				sessionStorage.setItem("message", JSON.stringify(data["message"]));
				sessionStorage.setItem("winner", JSON.stringify(data["winner"]));
				sessionStorage.setItem("game", JSON.stringify(data["game"]));
			}
		},
		error:  function (error) {
			console.log(error);
		}
	});	
}