
$( document ).ready(function() {
	$('#open_game').on('submit', function (e) {
	    e.preventDefault();

	    var data = $('#open_game').serializeArray();
	    var id = data[0].value;

	  	data = convertFormToJson(data);
	  	routeName  = "api/game/"+id;

	  	route(routeName, "GET", data, "game");
	  	setTimeout(function() {
  			window.location.replace("game-details.html");
		}, 1000);
	});
});

function listAllGames() {
	route("api/games", "GET", "{'blank':'pera'}", "games");
	setTimeout(function() {
  			gamesToDropDown();
		}, 1000);
}