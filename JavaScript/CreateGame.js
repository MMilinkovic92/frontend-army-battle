$( document ).ready(function() {
	$('#create_game_form').on('submit', function (e) {
	    e.preventDefault();

	    var data = $('#create_game_form').serializeArray();
	  	data = convertFormToJson(data);
	  	console.log(data);

	  	route("api/game/create", "POST", data);
	});
});