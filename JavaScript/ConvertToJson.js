function convertFormToJson(data) {
	var jsonObject = {};
	
	
	$.each(data, function( index, element ) {
		var key = element.name;
		jsonObject[key] = element.value;
	});

	return JSON.stringify(jsonObject);
}