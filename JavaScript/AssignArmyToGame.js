$( document ).ready(function() {
	$('#assign-army-to-game').on('submit', function (e) {
	    e.preventDefault();

	    var data = $('#assign-army-to-game').serializeArray();
	  	data = convertFormToJson(data);
	  	route("api/game/assign-army", "POST", data);
	});
});


function listArmiesAndGames(argument) {
	route("api/games", "GET", "", "games");
	route("api/armies", "GET", "", "armies");
	setTimeout(function() {
  			gamesToDropDown();
			armiesToDropDown();
		}, 1000);
}

function gamesToDropDown() {
	var games = getFromSession("games");
	$.each(games, function (index, object) {
		createDropdownOption(object.id, object.name, '#game');
	});
}

function armiesToDropDown() {
	var armies = getFromSession("armies");

	$.each(armies, function (index, object) {
		createDropdownOption(object.id, object.name, '#army');
	});
}


function getFromSession(key) {
	return JSON.parse(sessionStorage.getItem(key));
}


function createDropdownOption(objectId, objectName, dropdownId) {
	$(dropdownId).append(new Option(objectName,objectId));
}